package com.example.colormatrix.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrix.R
import com.example.colormatrix.databinding.FragmentColorBinding
import com.example.colormatrix.viewmodel.ColorViewModel

class ColorFragment : Fragment() {
    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.colorButton.setOnClickListener {
//
            colorViewModel.getColors(binding.etCounter.text.toString().toInt())
        }
        initObservers()
    }

    private fun initObservers() = with(colorViewModel) {
        colors.observe(viewLifecycleOwner) { colors ->
            binding.colorGrid.apply {
                layoutManager = GridLayoutManager(context, 3)
                adapter = ColorsAdapter().apply {
                    addColors(colors)
                    addItemClickListener { color : Int ->
                        findNavController().navigate(ColorFragmentDirections.actionColorFragmentToColorIntFragment(color))
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}