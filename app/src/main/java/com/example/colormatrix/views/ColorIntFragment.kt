package com.example.colormatrix.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.colormatrix.R
import com.example.colormatrix.databinding.FragmentColorIntBinding

class ColorIntFragment : Fragment() {
    private var _binding: FragmentColorIntBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<ColorIntFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorIntBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("colorINT", args.colorInt.toString())

        binding.colorIntGrid.apply {
            val colorIntList = args.colorInt.toString().split("")
            layoutManager = LinearLayoutManager(context)
            adapter = ColorsIntAdapter().apply {
                addColorsInt(colorIntList)
            }
        }
    }
}