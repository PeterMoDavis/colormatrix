package com.example.colormatrix.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrix.databinding.ItemColorBinding

class ColorsAdapter : RecyclerView.Adapter<ColorsAdapter.ColorsViewHolder>() {

    private var colors = mutableListOf<Int>()
    private var adapterListener: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ColorsViewHolder {
        val binding = ItemColorBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ColorsViewHolder(binding)
    }


    override fun onBindViewHolder(colorsViewHolder: ColorsViewHolder, position: Int) {
        val color = colors[position]
        colorsViewHolder.loadColor(color)
        colorsViewHolder.setClickListener {
            adapterListener?.invoke(color)
        }
    }

    override fun getItemCount(): Int {
        return colors.size
    }

    fun addColors(colors: MutableList<Int>) {
        this.colors = colors.toMutableList()
        notifyDataSetChanged()
    }

    fun addItemClickListener(function: (Int) -> Unit) {
        adapterListener = function
    }

    class ColorsViewHolder(
        private val binding: ItemColorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(color: Int) {
            binding.imageItem.setBackgroundColor(color)
        }

        fun setClickListener(clickListener: View.OnClickListener?) {
            binding.imageItem.setOnClickListener(clickListener)
        }
    }
}