package com.example.colormatrix.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrix.databinding.ItemIntBinding

class ColorsIntAdapter : RecyclerView.Adapter<ColorsIntAdapter.ColorsIntViewHolder>() {

    private var colorsInt = mutableListOf<String>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ColorsIntViewHolder {
        val binding = ItemIntBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ColorsIntViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ColorsIntAdapter.ColorsIntViewHolder, position: Int) {
        val colorInt = colorsInt[position]
        holder.loadColorInt(colorInt)
    }

    override fun getItemCount(): Int {
        return colorsInt.size
    }

    fun addColorsInt(colors: List<String>) {
        this.colorsInt = colors.toMutableList()
        notifyDataSetChanged()
    }

    class ColorsIntViewHolder(
        private val binding: ItemIntBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColorInt(colorInt: String) {
            binding.textItem.text = colorInt
        }
    }
}