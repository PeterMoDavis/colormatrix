package com.example.colormatrix.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colormatrix.model.ColorsRepo
import kotlinx.coroutines.*

class ColorViewModel : ViewModel() {
    private val repo = ColorsRepo
    private val _colors = MutableLiveData<MutableList<Int>>()
    val colors: LiveData<MutableList<Int>> get() = _colors

    fun getColors(num: Int) = viewModelScope.launch(Dispatchers.Main) {
        val colors = repo.getColor(num)
        _colors.value = colors
    }
}