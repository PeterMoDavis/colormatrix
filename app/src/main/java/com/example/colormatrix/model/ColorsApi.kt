package com.example.colormatrix.model

interface ColorsApi {
    suspend fun getColors(amount: Int): MutableList<Int>
}