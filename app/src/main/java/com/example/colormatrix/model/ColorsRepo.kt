package com.example.colormatrix.model

import android.graphics.Color
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.util.*

object ColorsRepo {
    private val colorsApi = object : ColorsApi {
        override suspend fun getColors(amount: Int): MutableList<Int> {
            var colors = mutableListOf<Int>()
            for (i in 1..amount) {
                val rnd = Random()
                val color: Int =
                    Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
                colors.add(color)
            }
            return colors
        }
    }

    suspend fun getColor(amount: Int): MutableList<Int> = withContext(Dispatchers.IO) {
        delay(2000)
        colorsApi.getColors(amount)
    }
}